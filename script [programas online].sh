#!/bin/bash

echo $password | sudo -S echo ""

sudo dpkg --add-architecture i386

wget -nc https://dl.winehq.org/wine-builds/Release.key
sudo apt-key add Release.key 

wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/ -y
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ xenial main' -y

sudo apt update -y

sudo apt install software-properties-common -y
sudo apt install apt-transport-https wget -y 
sudo apt install code -y
sudo apt install python3.7 -y
sudo apt install idle3 -y
sudo apt install libreoffice -y
sudo apt install default-jre -y
sudo apt install jclic -y
sudo apt install tuxmath -y
sudo apt install tuxpaint -y
sudo apt install tuxtype -y
sudo apt install scratch -y
sudo apt install squeak-vm -y
sudo apt install icedtea-netx -y
sudo apt install wine-stable -y
sudo apt install vlc -y
sudo apt install vim -y
sudo apt install gparted -y
sudo apt install browser-plugin-freshplayer-pepperflash -y
